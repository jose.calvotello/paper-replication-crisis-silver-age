# Repository  

This repository contains the code and data for the article:
- *Are the (Digital) Humanities Facing a Replication Crisis? Perspectives from the Spanish Silver Age(s)*, by José Calvo Tello and Nanette Rißler-Pipka (to be published)

# Paper about Edad de Plata
# CfP Details

https://hispanismo.cervantes.es/convocatorias-y-empleo/towards-digital-history-other-silver-age-spain-production-storage-use-and

CfP Towards the Digital History of the Other Silver Age Spain: Production, Storage, Use and Dissemination

## Details
Los investigadores interesados en participar deben mandar título y resumen de su propuesta de aproximadamente 300 a 400 palabras en inglés o castellano a Dolores Romero López (dromero@filol.ucm.es) y a Jeffrey Zamostny (jzamostn@westga.edu) antes del 15 de octubre de 2020. 

## Final proposal


Información adicional: 
El capítulo de aproximadamente 6.000 a 8.000 palabras en inglés (preferiblemente) o castellano se entregará antes del 15 de mayo de 2021. Los editores supervisarán la traducción al inglés de capítulos entregados en castellano.


## Language
- English

# Very Basic Idea

If researcher keep going not defining their research objects, DH might face a replication crisis.

# First-Short Proposal (15 de octubre de 2020, 300 a 400 palabras = media página)
Kürzungsbedarf: aktuell (12.10.) 607 Wörter...

## Are the Digital Humanities Facing a Replication Crisis? Perspectives from the Spanish Silver Age(s)
Many Digital Humanities (DH) areas have prioritised their interest and outcome in showing the potential of applying new methods to cultural data sets. However, after several decades of application, DH can be considered as an established discipline, which is observable in professorships, formal study programs, associations, research infrastructures, degree of penetration in many countries and specific universities, etc. Da's critic article (2019) about DH in general proposed some good questions about the acceptability of DH results beyond methodological questions. DH is able to produce new knowledge about tools and formats, but what is DH really discovering about their research object? What evidence can be given drawn out of a wider set of data (literature, cultural artefacts, etc)?

Several approaches from Computational Literary Studies have embraced a much larger scale of analysis regarding literature. Concepts like *Distant Reading*, *Macroanalysis* or *Cultural Analytics* are moving in this trend (Jockers, 2013; Moretti, 2013; Underwood, 2019). Besides, the newest methods in computer studies are increasingly data-hungrier, such as Word Embeddings or Deep Learning (Kestemont and De Gussem, 2016; Jannidis et al., 2019). The interest of applying those to data from the humanities encourages the effort to achieve as many texts as possible. However, previously, literary studies have not analysed  such an amount of data. The risk that selection is rather determined by the availability and accessibility of data but out of scientific criteria is rather a common problem. Two scholars working on literature of the same period and language might be analysing very different data sets. The selection of the entities that compound our research object have a major impact in the latter results. By that, DH could be facing in the future a similar replication and trust crisis (Schöch, 2017) as it has happened in last decade in other fields (Gómez et al., 2010; Achenbach, 2015; Baker, 2016; Munafò et al., 2017; Hutson, 2018).

The Spanish Silver Age presents an interesting case of study since its borders have been put in question by literary and cultural history. The concept of "the Other Silver Age" represents a wider horizon of analysis, moving along with digital tendencies (Romero López, 2014) and several projects have been active in the application of DH methods to this period (Ehrlicher and Rißler-Pipka, 2014; Romero López, 2014; Santos Zas and Vílchez Ruiz, 2017; Isasi, 2017; Calvo Tello, 2019; Santa María et al., 2020). Of course, this concept is opposed implicitely to "The Silver Age" in the context of literary and cultural history (García de Nora, 1963; Urrutia Cárdenas, 1999; Mainer, 2009). The characteristics and development of the debate on the Spanish Silver Age show the urgent need to discuss selection criteria and internal bias of data selection. It is unclear for both labels which authors and texts should be considered, which it is based on the premises the researchers implicitely or explicitely rely on.

In our proposal, we will discuss these aspects, analysing them from different angles and an interdisciplaniry point of view. More specifically, we will treat following questions:

- Which are feasible ways of defining reserach objects in the DH? What bias could they imply? Are we able to define the selection criteria and make the bias visible?
- To what degree the results of an analysis might vary depending on the selection, in specific and realistic cases? 
- What methodological and institutional steps are necessary to avoid a replication crisis in the Digital Humanities?

# Final and Long Proposal (15 de mayo de 2021, 6.000 a 8.000 palabras = 7 páginas)

Siehe Drive:  https://docs.google.com/document/d/1daS0tuI3r9UlXFCqQ5OL7VWfThFx3Fbk7VN1ZPAj114/edit?usp=sharing

